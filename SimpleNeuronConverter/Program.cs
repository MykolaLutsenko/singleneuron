﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleNeuronConverter
{
    class Program
    {
        static void Main(string[] args)
        {
            string s = null;
            decimal km = 100;
            decimal miles = 62.1371m;
            var stopwach = new Stopwatch();

            Neuron neuron = new Neuron();
            stopwach.Start();
            int i = 0;
            do
            {
                i++;
                neuron.Train(km,miles);
                Console.WriteLine($"Итерация: {i}\tОшибка:\t{neuron.LastError}");

            } while (neuron.LastError>neuron.Smoothing||neuron.LastError<-neuron.Smoothing);
            stopwach.Stop();
            string str =
                String.Format(
                    $"{stopwach.Elapsed.Hours:00}:{stopwach.Elapsed.Minutes:00}:{stopwach.Elapsed.Seconds:00}.{stopwach.Elapsed.Milliseconds / 10:00}");
            Console.WriteLine("Обучение завершено! Затрачено времени: " + str);

            Console.WriteLine($"{neuron.ProcessInputData(100)} миль в {100} км.");
            Console.WriteLine($"{neuron.ProcessInputData(541)} миль в {541} км.");
            Console.WriteLine($"{neuron.RestoreInputData(10)} км в {10} миль.");
        }
    }
}
